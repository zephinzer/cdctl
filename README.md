# `cdctl`

A CLI tool for retrieving information from Crypto.com

## Installation

Clone this repository locally and run `go install ./cmd/cdctl`

You should be able to use `cdctl --help` to get to where you want.
