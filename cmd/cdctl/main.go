package main

import (
	"os"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"gitlab.com/zephinzer/cdctl/cmd/cdctl/_utils/flags"
	"gitlab.com/zephinzer/cdctl/cmd/cdctl/get"
)

var conf = config.Map{
	flags.ApiKey:    flags.ApiKeyConfig,
	flags.SecretKey: flags.SecretKeyConfig,
}

var command = cobra.Command{
	Use: "cdctl",
	RunE: func(cmd *cobra.Command, args []string) error {
		logrus.Trace("executing command...")

		return nil
	},
}

func main() {
	logrus.SetOutput(os.Stderr)
	logrus.Trace("starting process...")
	conf.ApplyToCobraPersistent(&command)
	command.AddCommand(get.GetCommand())
	if err := command.Execute(); err != nil {
		logrus.Errorf("failed to complete successfully: %s", err)
	}
}
