package account

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"gitlab.com/zephinzer/cdctl/cmd/cdctl/_utils/flags"
	"gitlab.com/zephinzer/cdctl/pkg/client"
)

var conf = config.Map{
	"output": &config.String{
		Shorthand: "o",
		Usage:     "output format",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use: "account",
		Long: strings.Trim(`

This command retrieves your account information.

This is an authenticated endpoint so you will need to specify your Crypto.com API
key as well as secret key via the API_KEY and SECRET_KEY environment variables or
via flags as --api-key xxx and --secret-key yyy

Example with flags (not recommended):

	# run the following:
	# cdctl get account --api-key xxx --secret-key yyy

Example with environment variables:

	# put the following in a file to prevent it from showing up in command history
	# API_KEY=xxx
	# SECRET_KEY=yyy

	# then run the following:
	# source ./path/to/file
	# cdctl get account

`, "\n"),
		Aliases: []string{"a"},
		Short:   "Retrieves account information from Crypto.com",
		RunE:    runE,
	}

	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	secret, err := command.Flags().GetString(flags.SecretKey)
	if err != nil {
		return fmt.Errorf("failed to get secret key: %s", err)
	} else if len(secret) == 0 {
		return fmt.Errorf("failed to receive a non-empty secret")
	}
	apiKey, err := command.Flags().GetString(flags.ApiKey)
	if err != nil {
		return fmt.Errorf("failed to get api key: %s", err)
	} else if len(secret) == 0 {
		return fmt.Errorf("failed to receive a non-empty api key")
	}

	accounts, err := client.GetAccountSummary(
		client.GetAccountSummaryOpts{
			PrivateRequest: client.NewPrivateRequest(apiKey, secret),
		},
	)

	switch conf.GetString("output") {
	case "json":
		prettyResponseBody, err := json.MarshalIndent(accounts, "", "  ")
		if err != nil {
			return fmt.Errorf("failed to get pretty version of response: %s", err)
		}
		fmt.Println(string(prettyResponseBody))
	case "csv":
		fmt.Println("currency,balance,available,in order,staked")
		for _, a := range accounts {
			fmt.Printf("%s,%v,%v,%v,%v\n",
				a.Currency,
				a.Balance,
				a.Available,
				a.Order,
				a.Stake,
			)
		}
	default:
		for _, a := range accounts {
			fmt.Printf("%s\t%v\n", a.Currency, a.Balance)
		}
	}
	return nil
}
