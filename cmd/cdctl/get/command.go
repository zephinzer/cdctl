package get

import (
	"github.com/spf13/cobra"
	"gitlab.com/zephinzer/cdctl/cmd/cdctl/get/account"
	"gitlab.com/zephinzer/cdctl/cmd/cdctl/get/orders"
	"gitlab.com/zephinzer/cdctl/cmd/cdctl/get/price"
)

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:     "get",
		Aliases: []string{"g"},
		Short:   "Retrieves information",
		RunE:    runE,
	}

	command.AddCommand(account.GetCommand())
	command.AddCommand(price.GetCommand())
	command.AddCommand(orders.GetCommand())
	return &command
}

func runE(command *cobra.Command, args []string) error {
	return nil
}
