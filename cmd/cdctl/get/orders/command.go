package orders

import (
	"encoding/json"
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"gitlab.com/zephinzer/cdctl/cmd/cdctl/_utils/flags"
	"gitlab.com/zephinzer/cdctl/pkg/client"
	"gitlab.com/zephinzer/cdctl/pkg/types"
)

var conf = config.Map{
	"from-days-ago": &config.Uint{
		Shorthand: "f",
		Usage:     "from number of days ago",
		Default:   7,
	},
	"to-days-ago": &config.Uint{
		Shorthand: "t",
		Usage:     "to number of days ago",
		Default:   0,
	},
	"include-statuses": &config.StringSlice{
		Shorthand: "i",
		Usage:     "include these comma-delimited statuses",
	},
	"exclude-statuses": &config.StringSlice{
		Shorthand: "I",
		Usage:     "exclude these comma-delimited statuses",
	},
	"include-instruments": &config.StringSlice{
		Shorthand: "j",
		Usage:     "include these comma-delimited instruments",
	},
	"exclude-instruments": &config.StringSlice{
		Shorthand: "J",
		Usage:     "exclude these comma-delimited instruments",
	},
	"output": &config.String{
		Shorthand: "o",
		Usage:     "output format",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:     "order",
		Aliases: []string{"orders", "o"},
		Short:   "Retrieves trading information (requires api+secret key from crypto.com)",
		RunE:    runE,
	}

	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	secret, err := command.Flags().GetString(flags.SecretKey)
	if err != nil {
		return fmt.Errorf("failed to get secret key: %s", err)
	} else if len(secret) == 0 {
		return fmt.Errorf("failed to receive a non-empty secret")
	}
	apiKey, err := command.Flags().GetString(flags.ApiKey)
	if err != nil {
		return fmt.Errorf("failed to get api key: %s", err)
	} else if len(secret) == 0 {
		return fmt.Errorf("failed to receive a non-empty api key")
	}
	orders, err := client.GetOrderHistory(client.GetOrderHistoryOpts{
		PrivateRequest: client.NewPrivateRequest(apiKey, secret),
		FromDaysAgo:    conf.GetUint("from-days-ago"),
		ToDaysAgo:      conf.GetUint("to-days-ago"),
	})

	var filteredOrders []types.Order
	statusAllowlist := conf.GetStringSlice("include-statuses")
	statusBlocklist := conf.GetStringSlice("exclude-statuses")
	instrumentAllowlist := conf.GetStringSlice("include-instruments")
	instrumentBlocklist := conf.GetStringSlice("exclude-instruments")

	for _, order := range orders {
		if len(statusAllowlist) > 0 {
			allowed := false
			for _, allowedStatus := range statusAllowlist {
				if order.Status == allowedStatus {
					allowed = true
				}
			}
			if !allowed {
				continue
			}
		}
		if len(statusBlocklist) > 0 {
			blocked := false
			for _, blockedStatus := range statusAllowlist {
				if order.Status == blockedStatus {
					blocked = true
				}
			}
			if blocked {
				continue
			}
		}
		if len(instrumentAllowlist) > 0 {
			allowed := false
			for _, allowedInstrument := range instrumentAllowlist {
				if strings.Contains(order.InstrumentName, allowedInstrument) {
					allowed = true
				}
			}
			if !allowed {
				continue
			}
		}
		if len(instrumentBlocklist) > 0 {
			blocked := false
			for _, blockedInstrument := range instrumentBlocklist {
				if strings.Contains(order.InstrumentName, blockedInstrument) {
					blocked = true
				}
			}
			if blocked {
				continue
			}
		}
		filteredOrders = append(filteredOrders, order)
	}

	if err != nil {
		return fmt.Errorf("failed to get order history: %s", err)
	}
	switch conf.GetString("output") {
	case "json":
		ordersJson, err := json.MarshalIndent(filteredOrders, "", "  ")
		if err != nil {
			return fmt.Errorf("failed to marshal orders into json: %s", err)
		}
		fmt.Println(string(ordersJson))
	case "csv":
		delimiter := ","
		columns := []string{
			"coin+",
			"coin-",
			"completed at",
			"status",
			"direction",
			"quantity",
			"average price",
			"total",
		}
		fmt.Println(strings.Join(columns, delimiter))

		for _, order := range filteredOrders {
			coins := strings.Split(order.InstrumentName, "_")
			coinPlus := coins[0]
			coinMinus := coins[1]

			timestamp := time.Unix(
				0,
				int64(order.UpdateTime)*int64(time.Millisecond),
			).Format("2 Jan 2006 3:04:05 PM")
			cumulativeQuantity := strconv.FormatFloat(order.CumulativeQuantity, 'f', 8, 64)
			if order.Side == "SELL" {
				cumulativeQuantity = "-" + cumulativeQuantity
			}
			averagePrice := strconv.FormatFloat(order.AveragePrice, 'f', 8, 64)
			cumulativeValue := strconv.FormatFloat(order.CumulativeValue, 'f', 8, 64)
			if order.Side == "BUY" {
				cumulativeValue = "-" + cumulativeValue
			}
			values := []string{
				coinPlus,
				coinMinus,
				timestamp,
				order.Status,
				order.Side,
				cumulativeQuantity,
				averagePrice,
				cumulativeValue,
			}
			fmt.Println(strings.Join(values, delimiter))
		}
	default:
		for _, order := range filteredOrders {
			coins := strings.Split(order.InstrumentName, "_")
			coinPlus := coins[0]
			coinMinus := coins[1]
			var message strings.Builder
			if order.Side == "BUY" {
				switch order.Status {
				case "CANCELLED":
					message.WriteString("Cancelled buying ")
				case "REJECTED":
					message.WriteString("Rejected buying ")
				default:
					message.WriteString("Bought ")
				}
			} else {
				switch order.Status {
				case "CANCELLED":
					message.WriteString("Cancelled selling ")
				case "REJECTED":
					message.WriteString("Rejected selling ")
				default:
					message.WriteString("Sold ")
				}
			}
			message.WriteString(fmt.Sprintf("%v %s ", order.CumulativeQuantity, coinPlus))
			message.WriteString(fmt.Sprintf("at %v %s/%s ", order.AveragePrice, coinMinus, coinPlus))
			sign := "+"
			if order.Side == "BUY" {
				sign = "-"
			}
			message.WriteString(fmt.Sprintf("(total: %s%v %s) ", sign, order.CumulativeValue, coinMinus))
			transactionCompletedAt := time.Unix(0, int64(order.UpdateTime)*int64(time.Millisecond))
			message.WriteString(fmt.Sprintf("at %s on %s", transactionCompletedAt.Format("03:04:05 PM"), transactionCompletedAt.Format("Monday, January 2, 2006")))
			fmt.Println(message.String())
		}
	}

	return nil
}
