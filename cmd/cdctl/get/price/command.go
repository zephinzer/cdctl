package price

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/usvc/go-config"
	"gitlab.com/zephinzer/cdctl/pkg/constants"
	"gitlab.com/zephinzer/cdctl/pkg/endpoint"
	"gitlab.com/zephinzer/cdctl/pkg/types"
)

var conf = config.Map{
	"output": &config.String{
		Shorthand: "o",
		Usage:     "output format",
	},
}

func GetCommand() *cobra.Command {
	command := cobra.Command{
		Use:     "price",
		Aliases: []string{"p"},
		Short:   "Retrieves price information",
		RunE:    runE,
	}

	conf.ApplyToCobra(&command)
	return &command
}

func runE(command *cobra.Command, args []string) error {
	if len(args) != 2 {
		return fmt.Errorf("failed to receive two tickers")
	}
	code1 := strings.ToUpper(args[0])
	code2 := strings.ToUpper(args[1])
	tickerPair := code1 + "_" + code2
	apiUrl, err := constants.GetApiUrl(constants.CryptoDotComV2BaseUrl, "public/get-ticker", map[string]string{
		"instrument_name": tickerPair,
	})
	if err != nil {
		return fmt.Errorf("failed to construct url: %s", err)
	}
	logrus.Infof("calling endpoint '%s'", apiUrl)
	responseData, err := endpoint.Call(endpoint.CallOpts{
		RawUrl: apiUrl,
		Method: http.MethodGet,
		Body:   nil,
	})
	if err != nil {
		return fmt.Errorf("failed to call endpoint: %s", err)
	}
	var tickerData types.PublicTickerResponse
	if err := json.Unmarshal(responseData.Body, &tickerData); err != nil {
		return fmt.Errorf("failed to unmarshal response: %s", err)
	}

	switch conf.GetString("output") {
	case "json":
		prettyJson, err := json.MarshalIndent(tickerData.Result, "", "  ")
		if err != nil {
			return fmt.Errorf("failed to marshal json into a pretty format: %s", err)
		}
		logrus.Info("following is the data in json format")
		fmt.Println(string(prettyJson))
	case "csv":
		logrus.Info("following is the data in csv format")
		fmt.Printf("instrument,bid,ask,last,high,low,volume\n")
		fmt.Printf("%s,%v,%v,%v,%v,%v,%v\n",
			tickerData.Result.InstrumentName,
			tickerData.Result.BestBidPrice,
			tickerData.Result.BestAskPrice,
			tickerData.Result.LatestTradePrice,
			tickerData.Result.DailyHighPrice,
			*tickerData.Result.DailyLowPrice,
			tickerData.Result.DailyTradeVolume,
		)
	default:
		fmt.Printf("\033[1mINSTRUMENT: %s\033[0m\n", tickerData.Result.InstrumentName)
		fmt.Printf("VOL: %v\n", tickerData.Result.DailyTradeVolume)
		fmt.Printf("BID: %v\n", tickerData.Result.BestBidPrice)
		fmt.Printf("ASK: %v\n", tickerData.Result.BestAskPrice)
		fmt.Printf("LAST: %v\n", tickerData.Result.LatestTradePrice)
		fmt.Printf("HIGH: %v\n", tickerData.Result.DailyHighPrice)
		fmt.Printf("LOW: %v\n", *tickerData.Result.DailyLowPrice)
	}
	return nil
}
