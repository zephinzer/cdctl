package flags

import "github.com/usvc/go-config"

const ApiKey = "api-key"

var ApiKeyConfig = &config.String{
	Shorthand: "a",
	Usage:     "api key provided by crypto.com/exchange",
}

const SecretKey = "secret-key"

var SecretKeyConfig = &config.String{
	Shorthand: "s",
	Usage:     "secret key provided by crypto.com/exchange",
}
