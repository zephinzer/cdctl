/**
 * This is a modified JS sample, set API_KEY and API_SECRET environment variables
 * to test this sample out
 * 
 * Reference: https://exchange-docs.crypto.com/spot/index.html#digital-signature
 */

const crypto = require("crypto-js");

const signRequest = (request_body, api_key, secret) => {
  const { id, method, params, nonce } = request_body;

  function isObject(obj) { return obj !== undefined && obj !== null && obj.constructor == Object; }
  function isArray(obj) { return obj !== undefined && obj !== null && obj.constructor == Array; }
  function arrayToString(obj) {
    return obj.reduce((a,b) => {
      return a + (
        isObject(b) ? objectToString(b) : (
          isArray(b) ? arrayToString(b) : b
        )
      );
    }, "");
  }
  function objectToString(obj) {
    return (
      obj == null ? "" :
        Object.keys(obj).sort().reduce((a, b) => {
          return a + b + (
            isArray(obj[b]) ? arrayToString(obj[b]) : (
              isObject(obj[b]) ? objectToString(obj[b]) : obj[b]
            )
          );
        }, "")
    );
  }

  const paramsString = objectToString(params);

  console.log("method  ", method);
  console.log("id      ", id);
  console.log("params  ", paramsString);
  console.log("nonce   ", nonce);
  
  const sigPayload = method + id + api_key + paramsString + nonce;
  console.log("payload ", sigPayload);
  request_body.sig = crypto.HmacSHA256(sigPayload, secret).toString(crypto.enc.Hex);
  console.log(request_body.sig)
};

const apiKey = process.env['API_KEY']; /* User API Key */
const apiSecret = process.env['API_SECRET']; /* User API Secret */

let request = {
  id: 11,
  method: "private/get-order-detail",
  api_key: apiKey,
  params: {
    arr: ["a", "b", "c"],
    order_id: 53287421324,
    test2: {
      a: 12345,
      b: 54321,
    },
    test1: {
      x: 1234,
      y: 4321,
    },
  },
  nonce: 1587846358253,
};

const requestBody = JSON.stringify(signRequest(request, apiKey, apiSecret));
