package security

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"sort"
	"strings"

	"gitlab.com/zephinzer/cdctl/pkg/types"
)

type SignOpts struct {
	Secret string `json:"-"`
	types.RequestBody
}

// Sign returns the signature that needs to be attached to all
// private/* endpoints as defined in crypto.com's api documentation
func Sign(opts SignOpts) string {
	return getSignature(createPayload(opts.Method, opts.ID, opts.Params, opts.ApiKey, opts.Secret, opts.Nonce), opts.Secret)
}

// getSignature returns the HMAC-SHA256 hash of the provided payload,
// signing it with the provided secret
func getSignature(payload, secret string) string {
	hmacSigner := hmac.New(sha256.New, []byte(secret))
	hmacSigner.Write([]byte(payload))
	return hex.EncodeToString(hmacSigner.Sum(nil))
}

// createPayload returns a string that is ordered and formatted
// correctly for a signature to be generated for crypto.com
func createPayload(method string, id int, params map[string]interface{}, apiKey string, secret string, nonce string) string {
	return fmt.Sprintf("%s%v%s%s%v", method, id, apiKey, stringifyParams(params), nonce)
}

// stringifyParams implements stringifying the parameters object
// so that a signature can be generated from that payload
func stringifyParams(object map[string]interface{}) string {
	var output strings.Builder
	keys := []string{}
	for key, _ := range object {
		keys = append(keys, key)
	}
	sort.Strings(keys)
	for i := 0; i < len(keys); i++ {
		key := keys[i]
		value := object[key]
		output.WriteString(key)
		if obj, ok := value.(map[string]interface{}); ok {
			output.WriteString(stringifyParams(obj))
			continue
		}
		output.WriteString(fmt.Sprintf("%v", value))
	}
	return output.String()
}
