package security

import (
	"testing"

	"github.com/stretchr/testify/suite"
	"gitlab.com/zephinzer/cdctl/pkg/types"
)

type SignTests struct {
	suite.Suite
}

func TestSign(t *testing.T) {
	suite.Run(t, &SignTests{})
}

func (s SignTests) Test_Sign() {
	output := Sign(SignOpts{
		Secret: "secret",
		RequestBody: types.RequestBody{
			ID:     1,
			Method: "method",
			ApiKey: "apikey",
			Nonce:  "nonce",
			Params: map[string]interface{}{
				"hello": "world",
			},
		},
	})

	s.Equal(
		"bedaf3e3d0af988b6cafcde94b6c5ad72a430472f13a7b5b2c84835e4daf3701",
		output,
		"computed signature does not equal pre-computed value",
	)
}

func (s SignTests) Test_getSignature() {
	output := getSignature("a", "b")
	s.Equal(
		"cb448b440c42ac8ad084fc8a8795c98f5b7802359c305eabd57ecdb20e248896",
		output,
		"computed signature does not equal pre-computed value",
	)
}

func (s SignTests) Test_createPayload() {
	output := createPayload("method", 12345, map[string]interface{}{"hello": "world"}, "apikey", "secret", "nonce")
	s.Equal(
		"method12345apikeyhelloworldnonce",
		output,
		"payload does not seem correctly ordered/contain everything",
	)
}

func (s SignTests) Test_stringifyParams_all() {
	output := stringifyParams(map[string]interface{}{
		"a": map[string]interface{}{
			"b": map[string]interface{}{
				"c": map[string]interface{}{
					"e": -2,
					"d": 1,
				},
			},
		},
	})

	s.Equal(
		"abcd1e-2",
		output,
		"something went wrong with this method",
	)
}

func (s SignTests) Test_stringifyParams_lexicalSort() {
	output := stringifyParams(map[string]interface{}{
		"b": "b",
		"a": "a",
	})

	s.Equal(
		"aabb",
		output,
		"keys of the object should be sorted and processed lexicographically",
	)
}
func (s SignTests) Test_stringifyParams_multilevel() {
	output := stringifyParams(map[string]interface{}{
		"a": map[string]interface{}{
			"b": map[string]interface{}{
				"c": map[string]interface{}{
					"d": map[string]interface{}{
						"e": "fuckyou",
					},
				},
			},
		},
	})

	s.Equal(
		"abcdefuckyou",
		output,
		"deeply nested params doesn't seem to be working correctly",
	)
}

func (s SignTests) Test_stringifyParams_typeSupport() {
	output := stringifyParams(map[string]interface{}{
		"a": map[string]interface{}{
			"string": "string",
			"int":    -42,
			"uint":   42,
			"bool":   false,
			"float":  1.512,
		},
	})

	s.Equal(
		"aboolfalsefloat1.512int-42stringstringuint42",
		output,
		"some types were not rendered correctly",
	)
}
