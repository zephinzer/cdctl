package endpoint

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
)

type CallOpts struct {
	RawUrl string
	Body   []byte
	Method string
}

type Response struct {
	Body       []byte
	Headers    map[string][]string
	StatusCode int
}

func Call(opts CallOpts) (*Response, error) {
	var request *http.Request
	var err error
	if opts.Body != nil {
		body := bytes.NewReader(opts.Body)
		request, err = http.NewRequest(opts.Method, opts.RawUrl, body)
	} else {
		request, err = http.NewRequest(opts.Method, opts.RawUrl, nil)
	}
	if err != nil {
		return nil, fmt.Errorf("failed to construct request: %s", err)
	}

	request.Header.Set("Content-Type", "application/json")

	response, err := http.DefaultClient.Do(request)
	if err != nil {
		return nil, fmt.Errorf("failed to perform request: %s", err)
	}

	responseBody, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read response body: %s", err)
	}

	return &Response{
		Body:       responseBody,
		Headers:    response.Header,
		StatusCode: response.StatusCode,
	}, nil
}
