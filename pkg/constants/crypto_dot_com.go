package constants

const (
	CryptoDotComUatV1BaseUrl = "https://uat-api.3ona.co/v1"
	CryptoDotComUatV2BaseUrl = "https://uat-api.3ona.co/v2"
	CryptoDotComV1BaseUrl    = "https://api.crypto.com/v1"
	CryptoDotComV2BaseUrl    = "https://api.crypto.com/v2"

	CryptoDotComUatV1WsUser   = "wss://uat-stream.3ona.co/v2/user"
	CryptoDotComUatV1WsMarket = "wss://uat-stream.3ona.co/v2/market"
	CryptoDotComV1WsUser      = "wss://stream.crypto.com/v2/user"
	CryptoDotComV2WsMarket    = "wss://stream.crypto.com/v2/market"
)
