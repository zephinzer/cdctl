package constants

import (
	"fmt"
	"net/url"
	"path"
	"strings"
)

func GetApiUrl(baseUrl, apiPath string, query ...map[string]string) (string, error) {
	protocolAndUri := strings.Split(baseUrl, "//")
	if len(protocolAndUri) != 2 {
		return "", fmt.Errorf("failed to receive a valid base url")
	}

	protocol := strings.Trim(protocolAndUri[0], ":")
	baseUri := strings.Trim(protocolAndUri[1], "/")
	targetPath := strings.Trim(apiPath, "/")

	targetUrl := protocol + "://" + path.Join(baseUri, "/"+targetPath)
	if len(query) > 0 {
		queryValues := url.Values{}
		for k, v := range query[0] {
			if queryValues[k] == nil {
				queryValues[k] = []string{}
			}
			queryValues[k] = append(queryValues[k], v)
		}
		targetUrl += "?" + queryValues.Encode()
	}

	return targetUrl, nil
}
