package client

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
	"time"

	"gitlab.com/zephinzer/cdctl/pkg/constants"
	"gitlab.com/zephinzer/cdctl/pkg/endpoint"
	"gitlab.com/zephinzer/cdctl/pkg/security"
	"gitlab.com/zephinzer/cdctl/pkg/types"
)

// GetOrderHistoryOpts configures GetOrderHistory
type GetOrderHistoryOpts struct {
	PrivateRequest

	// FromDaysAgo indicates the number of days of
	// trades to return
	FromDaysAgo uint

	// ToDaysAgo indicates the number of days to query up to
	ToDaysAgo uint

	// Interval indicates the interval between requests,
	// use this to play nice with the platform's rate-limit
	Interval time.Duration
}

// GetOrderHistory retrieves trade history information
func GetOrderHistory(opts GetOrderHistoryOpts) ([]types.Order, error) {
	if opts.Interval < time.Second { // official rate-limit is 1 req/second
		opts.Interval = time.Second
	}
	if opts.FromDaysAgo == 0 && opts.ToDaysAgo == 0 {
		opts.FromDaysAgo = 1
	}
	if opts.FromDaysAgo == opts.ToDaysAgo {
		opts.FromDaysAgo++
	}
	if opts.ToDaysAgo > opts.FromDaysAgo {
		return nil, fmt.Errorf("failed to receive a logical time range ('to' should be less than 'from')")
	}

	method := "private/get-order-history"
	apiUrl, err := constants.GetApiUrl(constants.CryptoDotComV2BaseUrl, method)
	if err != nil {
		return nil, fmt.Errorf("failed to create api url: %s", err)
	}

	orders := []types.Order{}
	for i := opts.ToDaysAgo; i < opts.FromDaysAgo+1; i++ {
		requestBody := types.RequestBody{
			ID:     0,
			Method: method,
			ApiKey: opts.PrivateRequest.ApiKey,
			Params: map[string]interface{}{
				"page_size": 200,
				"start_ts":  time.Now().Add(-1 * time.Hour * 24 * time.Duration(opts.FromDaysAgo-i)).UnixMilli(),
				"end_ts":    time.Now().Add(-1 * time.Hour * 24 * time.Duration(opts.FromDaysAgo-i-1)).UnixMilli(),
			},
			Nonce: fmt.Sprintf("%v", time.Now().UnixMilli()),
		}
		requestBody.Sig = security.Sign(security.SignOpts{
			Secret:      opts.PrivateRequest.SecretKey,
			RequestBody: requestBody,
		})
		requestJSON, err := json.Marshal(requestBody)
		if err != nil {
			return nil, fmt.Errorf("failed to marshal request body into json: %s", err)
		}
		response, err := endpoint.Call(endpoint.CallOpts{
			RawUrl: apiUrl,
			Method: http.MethodPost,
			Body:   requestJSON,
		})
		var responseData types.OrderHistoryResponse
		if err := json.Unmarshal(response.Body, &responseData); err != nil {
			return nil, fmt.Errorf("failed to unmarshal response body: %s (raw data: %s)", err, string(response.Body))
		}
		sortedOrders := types.TimeSortableOrders(responseData.Result.OrderList)
		sort.Sort(sortedOrders)
		orders = append(orders, sortedOrders...)
		<-time.After(opts.Interval)
	}

	return orders, nil
}
