package client

type PrivateRequest struct {
	// ApiKey is the API key provided by Crypto.com
	ApiKey string

	// SecretKey is the secret key provided by Crypto.com
	SecretKey string
}

func NewPrivateRequest(apiKey, secret string) PrivateRequest {
	return PrivateRequest{
		ApiKey:    apiKey,
		SecretKey: secret,
	}
}
