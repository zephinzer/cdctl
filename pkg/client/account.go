package client

import (
	"encoding/json"
	"fmt"
	"net/http"
	"time"

	"gitlab.com/zephinzer/cdctl/pkg/constants"
	"gitlab.com/zephinzer/cdctl/pkg/endpoint"
	"gitlab.com/zephinzer/cdctl/pkg/security"
	"gitlab.com/zephinzer/cdctl/pkg/types"
)

type GetAccountSummaryOpts struct {
	PrivateRequest

	// BalanceLimit indicates the threshold amount below
	// which the account will be hidden in the returned
	// value
	BalanceLimit float64
}

func GetAccountSummary(opts GetAccountSummaryOpts) ([]types.Account, error) {
	method := "private/get-account-summary"
	requestBody := types.RequestBody{
		ID:     0,
		Method: method,
		ApiKey: opts.PrivateRequest.ApiKey,
		Params: map[string]interface{}{},
		Nonce:  fmt.Sprintf("%v", time.Now().UnixMilli()),
	}
	requestBody.Sig = security.Sign(security.SignOpts{
		Secret:      opts.SecretKey,
		RequestBody: requestBody,
	})
	requestJSON, err := json.Marshal(requestBody)
	if err != nil {
		return nil, fmt.Errorf("failed to marshal request body into json: %s", err)
	}
	apiUrl, err := constants.GetApiUrl(constants.CryptoDotComV2BaseUrl, method)
	if err != nil {
		return nil, fmt.Errorf("failed to create api url: %s", err)
	}
	response, err := endpoint.Call(endpoint.CallOpts{
		RawUrl: apiUrl,
		Method: http.MethodPost,
		Body:   requestJSON,
	})
	var accountSummaryResponse types.AccountSummaryResponse
	if err := json.Unmarshal(response.Body, &accountSummaryResponse); err != nil {
		return nil, fmt.Errorf("failed to process account summary: %s", err)
	}
	var validAccounts []types.Account
	for _, account := range accountSummaryResponse.Accounts {
		if account.Balance+account.Stake+account.Order+account.Available > opts.BalanceLimit {
			validAccounts = append(validAccounts, account)
		}
	}
	return validAccounts, nil
}
