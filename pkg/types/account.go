package types

type AccountSummaryResponse struct {
	AccountSummaryResult `json:"result"`
	Response
}

type AccountSummaryResult struct {
	Accounts []Account `json:"accounts"`
}

type Account struct {
	Balance   float64 `json:"balance"`
	Available float64 `json:"available"`
	Order     float64 `json:"order"`
	Stake     float64 `json:"stake"`
	Currency  string  `json:"currency"`
}
