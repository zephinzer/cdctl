package types

type PublicTickerResponse struct {
	Result PublicTickerResult `json:"result"`
	Response
}

// PublicTickerResult wraps a PublicTicker dataset
// with Crypto.com's response structure
//
// Reference link: https://exchange-docs.crypto.com/spot/index.html#public-get-ticker
type PublicTickerResult struct {
	InstrumentName   string `json:"instrument_name"`
	PublicTickerData `json:"data"`
}

// PublicTickerData is the logical response data from
// the /public/get-ticker endpoint
//
// Reference link: https://exchange-docs.crypto.com/spot/index.html#public-get-ticker
type PublicTickerData struct {
	InstrumentName   string   `json:"i"`
	BestBidPrice     float64  `json:"b"`
	BestAskPrice     float64  `json:"k"`
	LatestTradePrice float64  `json:"a"`
	Timestamp        uint64   `json:"t"`
	DailyTradeVolume float64  `json:"v"`
	DailyHighPrice   float64  `json:"h"`
	DailyLowPrice    *float64 `json:"l"`
	DailyPriceChange float64  `json:"c"`
}
