package types

type RequestBody struct {
	ID     int                    `json:"id"`
	Method string                 `json:"method"`
	ApiKey string                 `json:"api_key"`
	Params map[string]interface{} `json:"params"`
	Nonce  string                 `json:"nonce"`
	Sig    string                 `json:"sig"`
}
