package types

// OrderHistoryResponse is the full response data structure
type OrderHistoryResponse struct {
	Result OrderHistoryResult `json:"result"`
	Response
}

// OrderHistoryResult defines the structure of the results property
type OrderHistoryResult struct {
	OrderList []Order `json:"order_list"`
}

// TimeSortableOrders is a sortable slice of Order instances
type TimeSortableOrders []Order

func (o TimeSortableOrders) Len() int {
	return len(o)
}

func (o TimeSortableOrders) Less(i, j int) bool {
	return o[i].UpdateTime < o[j].UpdateTime
}

func (o TimeSortableOrders) Swap(i, j int) {
	o[i], o[j] = o[j], o[i]
}

// Order defines the structure of a single order
type Order struct {
	AveragePrice       float64 `json:"avg_price"`
	ClientOid          string  `json:"client_oid"`
	CreateTime         uint64  `json:"create_time"`
	CumulativeQuantity float64 `json:"cumulative_quantity"`
	CumulativeValue    float64 `json:"cumulative_value"`
	ExecInst           string  `json:"exec_inst"`
	FeeCurrency        string  `json:"fee_currency"`
	InstrumentName     string  `json:"instrument_name"`
	OrderId            string  `json:"order_id"`
	Price              float64 `json:"price"`
	Quantity           float64 `json:"quantity"`
	Side               string  `json:"side"`
	Status             string  `json:"status"`
	TimeInForce        string  `json:"time_in_force"`
	Type               string  `json:"type"`
	UpdateTime         uint64  `json:"update_time"`
}
