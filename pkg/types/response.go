package types

// Response defines the bsae response
type Response struct {
	ID     int    `json:"id"`
	Code   int    `json:"code"`
	Method string `json:"method"`
}
